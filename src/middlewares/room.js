const { RoomModel } = require('../models/Room');
module.exports = {
  createDefaultRooms: async (req, res, next) => {
    try {
      const { component } = req;

      const varibales = ['variable_knowing', 'variable_destructive', 'variable_constructive', 'variable_resistance', 'variable_reinforce', 'variable_protection', 'variable_alternative', 'variable_healing', 'variable_growth', 'variable_vision'];
      let rooms = [];
      varibales.forEach((variable) => {
        rooms = [...rooms, RoomModel({ title: 'room_me', component, variable, order: 0 }),
          RoomModel({ title: 'room_visionary', component, variable, order: 1 }),
          RoomModel({ title: 'room_partnership', component, variable, order: 2 }),
          RoomModel({ title: 'room_family', component, variable, order: 3 }),
          RoomModel({ title: 'room_group', component, variable, order: 4 }),
          RoomModel({ title: 'room_entrepreneurship', component, variable, order: 5 }),
          RoomModel({ title: 'room_organization', component, variable, order: 6 }),
          RoomModel({ title: 'room_commune', component, variable, order: 7 }),
          RoomModel({ title: 'room_collective', component, variable, order: 8 })];
      });
      req.rooms = Promise.all(rooms.map(async r => await r.save()));
      next();
    } catch (e) {
      throw e;
    }
  },
};
