const { CategoryAModel } = require('../models/categoryA');

module.exports = {
  createCategoryA: async (req, res, next) => {
    const { categoryTitle } = req;
    try {
      const categoryA = await CategoryAModel.create({ title: categoryTitle });
      req.categoryA = categoryA;
      next();
    } catch (e) {
      throw e;
    }

  },
  checkCategories: async (req, res, next) => {
    let categories = await CategoryAModel.find({
        title: {
          $in: ['cat_a_inner_world', 'cat_a_interaction', 'cat_a_outside_world', 'cat_a_whole'],
        },
      },
    );
    if (categories.length === 0) {
      categories =  await Promise.all(['cat_a_inner_world', 'cat_a_interaction', 'cat_a_outside_world', 'cat_a_whole'].map(async t => {
        return CategoryAModel.create({ title: t });
      }));
    }
    req.categoryAList = categories;
    next();

  },
};
