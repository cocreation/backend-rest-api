const { CategoryBModel } = require('../models/categoryB');
const { CategoryAModel } = require('../models/categoryA');

module.exports = {
  createCategoryB: async (req, res, next) => {
    const { categoryTitle } = req;
    try {
      const categoryA = await CategoryAModel.create({ title: categoryTitle });
      req.categoryA = categoryA;
      next();
    } catch (e) {
      throw e;
    }

  },
  checkCategoryBList: async (req, res, next) => {
    let categories = await CategoryBModel.find({
        title: {
          $in: [
            'cat_b_esprit',
            'cat_b_soul',
            'cat_b_body',
            'cat_b_human',
            'cat_b_nature',
            'cat_b_outside_world',
            'cat_b_politics',
            'cat_b_economy',
            'cat_b_right',
            'cat_b_education',
            'cat_b_religions',
            'cat_b_decision_instances',
            'cat_b_military',
            'cat_b_technology',
            'cat_b_whole',
          ],
        },
      },
    );
    if (categories.length === 0) {
      const categoryAList = await Promise.all(['cat_a_inner_world', 'cat_a_interaction', 'cat_a_outside_world', 'cat_a_whole'].map(async t => {
        return CategoryAModel.findOne({ title: t });
      }));
      categories = await Promise.all([
        'cat_b_esprit',
        'cat_b_soul',
        'cat_b_body',
        'cat_b_human',
        'cat_b_nature',
        'cat_b_outside_world',
        'cat_b_politics',
        'cat_b_economy',
        'cat_b_right',
        'cat_b_education',
        'cat_b_religions',
        'cat_b_decision_instances',
        'cat_b_military',
        'cat_b_technology',
        'cat_b_whole',
      ].map(async c => {
        if (c === 'cat_b_esprit' || c === 'cat_b_soul' || c === 'cat_b_body') {
          return CategoryBModel.create({ title: c, parentCategoryA: categoryAList[0] });
        } else if (c === 'cat_b_human' || c === 'cat_b_nature' || c === 'cat_b_outside_world') {
          return CategoryBModel.create({ title: c, parentCategoryA: categoryAList[1] });
        } else if (c === 'cat_b_whole') {
          return Promise.all([
            CategoryBModel.create({ title: c, parentCategoryA: categoryAList[0] }),
            CategoryBModel.create({ title: c, parentCategoryA: categoryAList[1] }),
          ]);
        } else {
          return CategoryBModel.create({ title: c, parentCategoryA: categoryAList[2] });
        }
      }));
    }
    req.categoryBList = categories;
    next();

  },
};
