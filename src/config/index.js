/**
 * Created by milad on 10/28/17.
 */
module.exports = {
    applicationPort: process.env.PORT,
    mongoUrl: `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:27017/${process.env.MONGO_DB}`,
    bodyLimit: '900kb'
}
