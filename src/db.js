const mongoose = require('mongoose');
const config = require('./config');
const BlueBird = require('bluebird');

module.exports = async callback => {
  let db = mongoose.connect(config.mongoUrl, {
    promiseLibrary: BlueBird,
    useNewUrlParser: true,
    useCreateIndex: true,
  });

  callback(db);
};
