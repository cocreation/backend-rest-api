const mongoose = require('mongoose');
const { CategoryASchema } = require('./categoryA');
const { CategoryBSchema } = require('./categoryB');
const ComponentSchema = new mongoose.Schema({
  title: {},
  categoryA: {
    type: CategoryASchema,
  },
  categoryB: {
    type: CategoryBSchema,
  },
  categories: {
    type: [{
      categoryA: {
        type: CategoryASchema,
        required: true,
      },
      categoryB: {
        type: CategoryBSchema,
      },
    }],
  },
  languages: {
    type: [String],
    enum: ['en', 'de', 'fa', 'fr'],
    default: ['en'],
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  likes: {
    type: Number,
    default: 0,
  },
});

module.exports = {
  ComponentModel: mongoose.model('Component', ComponentSchema),
  ComponentSchema: ComponentSchema,
};
