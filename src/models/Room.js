const mongoose = require('mongoose');
const {ComponentSchema} = require('./component');
const RoomSchema = new mongoose.Schema({
    title: {
        type: String,
        //enum: ['destructive', 'constructive', 'me', 'we'],
    },
    component: {
        type: ComponentSchema,
        unique: false,
    },
    order: {
        type: Number,
        default: 0,
    },
    variable: {
        type: String,
        //enum: ['is_ambition', 'should_ambition', 'is_condition', 'should_condition'],
        required: true,
    },
    questions: [{
        title: {
            type: String,
            enum: ['q_is_condition_constructive_first', 'q_is_condition_constructive_second', 'q_is_condition_destructive_first', 'q_is_condition_destructive_second', 'q_is_amibition_me_first', 'q_is_amibition_me_second', 'q_is_amibition_we_first', 'q_is_amibition_we_second',
                'q_should_condition_me_first', 'q_should_condition_me_second', 'q_should_condition_we_first', 'q_should_condition_we_second', 'q_should_ambition_me_first', 'q_should_ambition_me_second', 'q_should_ambition_we_first', 'q_should_ambition_we_second'],
        },
        answers: {
            type: [String],
            default: [],
        },
    }],
    likes: {
        type: Number,
        default: 0,
    },
    posts: {
        type: [{
            coverImage: {
                type: mongoose.Mixed,
                default: {},
            },
            title: {
                type: String,
            },
            body: {
                type: String,
            },
            categories: {
                type: [String],
                enum: [
                    'Inform',
                    'Community',
                    'Instances',
                    'Initiatives',
                    'Events',
                    'Offer',
                    'Survey',
                    'Jobs',
                    'Source funding',
                    'Crowdfunding',
                    'Coworking'
                ],
                default: []
            },
            createdAt: {
                type: Date,
                default: Date.now,
            },
            deleted: {
                type: Boolean,
                default: false,
            },
        }],
        default: [],
    },
});

module.exports = {
    RoomModel: mongoose.model('Room', RoomSchema),
    RoomSchema: RoomSchema,
};
