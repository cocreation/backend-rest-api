const mongoose = require('mongoose');

const AuditLogSchema = new mongoose.Schema({
  action: {
    type: String,
    enum: ['like_category_a', 'like_category_b', 'like_component', 'like_room', 'like_variable_is_ambition', 'like_variable_should_ambition', 'like_variable_is_condition', 'like_variable_should_condition'],
  },
  issuerId: {
    type: mongoose.Schema.ObjectId,
  },
  object: {
    type: mongoose.Schema.ObjectId,
  },
  ipAddress: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = {
  AuditLogModel: mongoose.model('AuditLog', AuditLogSchema),
  AuditLogSchema: AuditLogSchema,
};
