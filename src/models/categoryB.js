const mongoose = require('mongoose');
const { CategoryASchema } = require('./categoryA');

const CategoryBSchema = new mongoose.Schema({
  title: {
    type: String,
    enum: [
      'cat_b_esprit',
      'cat_b_soul',
      'cat_b_body',
      'cat_b_human',
      'cat_b_nature',
      'cat_b_outside_world',
      'cat_b_politics',
      'cat_b_economy',
      'cat_b_right',
      'cat_b_education',
      'cat_b_religions',
      'cat_b_decision_instances',
      'cat_b_military',
      'cat_b_technology',
      'cat_b_whole',
    ],
  },
  parentCategoryA: {
    type: CategoryASchema,
  },
  likes:{
    type: Number,
    default: 0
  }
});

module.exports = {
  CategoryBModel: mongoose.model('CategoryB', CategoryBSchema),
  CategoryBSchema: CategoryBSchema,
};
