const mongoose = require('mongoose');

const LanguageSchema = new mongoose.Schema({
  languageName: {
    type: String,
    default: 'en',
    enum: ['en', 'de', 'fa'],
  },
  locales: {
    type: Object,
    default: {},
  },
});

module.exports = {
  LanguageModel: mongoose.model('Language', LanguageSchema),
  LanguageSchema: LanguageSchema,
};
