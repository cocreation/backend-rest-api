const mongoose = require('mongoose');

const CategoryASchema = new mongoose.Schema({
  title: {
    type: String,
    enum: ['cat_a_inner_world', 'cat_a_interaction', 'cat_a_outside_world', 'cat_a_whole'],
  },
  likes: {
    type: Number,
    default: 0,
  },
});

module.exports = {
  CategoryAModel: mongoose.model('CategoryA', CategoryASchema),
  CategoryASchema: CategoryASchema,
};
