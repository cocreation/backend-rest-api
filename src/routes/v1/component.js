const { Router } = require('express');
const { ComponentModel } = require('../../models/component.js');
const { createDefaultRooms } = require('../../middlewares/room');
const { CategoryBModel } = require('../../models/categoryB');
const { CategoryAModel } = require('../../models/categoryA');
const { AuditLogModel } = require('../../models/auditLog');


module.exports = ({ db, config }) => {
  const api = Router();

  api.get('/:categoryA/:categoryB', async (req, res) => {
    try {
      const { categoryA, categoryB } = req.params;
      const components = await ComponentModel.find({
        deleted: false,
        'categoryA._id': categoryA,
        'categoryB._id': categoryB,
      });
      res.status(200).json({
        isSuccess: true,
        message: 'List of Components',
        data: components,
      });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.get('/', async (req, res) => {
    try {
      const components = await ComponentModel.find();
      res.status(200).json({
        isSuccess: true,
        message: 'List of Components',
        data: components,
      });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.get('/:id', async (req, res) => {
    try {
      const component = await ComponentModel.findById(req.params.id);
      res.status(200).json({
        isSuccess: true,
        message: 'component',
        data: component,
      });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.get('/:categoryA', async (req, res) => {
    try {
      const { categoryA } = req.params;
      const components = await ComponentModel.find({ deleted: false, 'categoryA._id': categoryA });
      res.status(200).json({
        isSuccess: true,
        message: 'List of Components',
        data: components,
      });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.post('/', async (req, res, next) => {
    try {

      const categories = await Promise.all(req.body.categories.map(c => {
        const categoryA = CategoryAModel.findById(c.categoryA);
        const categoryB = CategoryBModel.findById(c.categoryB);
        return {
          categoryA,
          categoryB,
        };
      }));
      const component = ComponentModel({
        title: { [req.body.languages[0]]: req.body.title },
        categories: req.body.categories,
        languages: req.body.languages,
      });

      console.log(component);
      const result = await component.save();
      req.component = result;
      next();
      // res.status(201).json({
      //   isSuccess: true,
      //   message: 'Component Created Successfully',
      //   data: result,
      // });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error.message,
      });
    }
  }, createDefaultRooms, async (req, res) => {
    const { component, rooms } = req;
    // component.rooms = rooms;
    // await component.save();
    res.status(201).json({
      isSuccess: true,
      message: 'Component Created Successfully',
      data:component
    });
  });

  api.post('/:id/like', async (req, res) => {
    try {
      const component = await ComponentModel.findById(req.params.id);
      if (component) {
        component.likes += 1;
        await component.save();
        await AuditLogModel.create({
          action: 'like_component',
          ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
          object: component._id,
        });
        res.status(200)
          .json({
            isSuccess: true,
            data: component,
            message: 'liked!',
          });
      } else {
        res.status(404)
          .json({
            isSuccess: false,
            data: {},
            message: 'Component not found',
          });
      }

    } catch (e) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: e,
      });
    }
  });

  api.post('/:id/contribute', async (req, res) => {
    try {
      const result = await ComponentModel.updateOne({
        _id: req.params.id,
        languages: { $nin: [req.body.language] },
      }, {
        $push: {
          languages: req.body.language,
        },
        $set: {
          [`title.${req.body.language}`]: req.body.title,
        },
      });
      if (result.nModified !== 0) {
        res.status(201)
          .json({
            isSuccess: true,
            data: result,
            message: 'contribution success',
          });
      } else {
        res.status(403)
          .json({
            isSuccess: false,
            data: result,
            message: 'before contributed!',
          });
      }

    } catch (e) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: e,
      });
    }
  });

  return api;
};
