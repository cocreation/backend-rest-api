const express = require('express');
const initDb = require('../../db');
const config = require('../../config');
const cors = require('cors');
const ComponentRouter = require('./component');
const RoomRouter = require('./room');
const CategoryRouter = require('./category');
const VarialeRouter = require('./variable');
const router = express();

initDb(db => {
  router.use(
    cors({
      origin: '*',
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    }),
  );
  router.use('/component', ComponentRouter({ db, config }));
  router.use('/room', RoomRouter({ db, config }));
  router.use('/category', CategoryRouter({ db, config }));
  router.use('/variable', VarialeRouter({ db, config }));
});

module.exports = router;
