const { Router } = require('express');
const { ComponentModel } = require('../../models/component.js');
const { RoomModel } = require('../../models/Room');
const { AuditLogModel } = require('../../models/auditLog');
const multer = require('multer');
const upload = multer({ dest: __dirname + '/../../public/images/' });


module.exports = ({ db, config }) => {
  const api = Router();

  api.get('/:component/:variable/list', async (req, res) => {
    try {
      const rooms = await RoomModel.find({
        'component._id': req.params.component,
        variable: req.params.variable,
      }).sort({ order: 1 });
      res.status(200)
        .json({
          isSuccess: true,
          message: 'Rooms in the component',
          data: rooms,
        });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.post('/', async (req, res) => {
    try {
      const component = await ComponentModel.findById(req.body.component);
      if (component) {
        const room = RoomModel({
          title: req.body.title,
          component,
          variable: req.body.variable,
        });
        const result = await room.save();
        res.status(201)
          .json({
            isSuccess: true,
            message: 'Room Created Successfully',
            data: result,
          });
      } else {
        res.status(400)
          .json({
            isSuccess: false,
            message: 'Invalid Component',
            data: {},
          });
      }
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: error,
      });
    }
  });

  api.post('/:id/like', async (req, res) => {
    try {
      const room = await RoomModel.findById(req.params.id);
      if (room) {
        room.likes += 1;
        await room.save();
        await AuditLogModel.create({
          action: 'like_room',
          ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
          object: room._id,
        });
        res.status(200)
          .json({
            isSuccess: true,
            data: room,
            message: 'liked!',
          });
      } else {
        res.status(404)
          .json({
            isSuccess: false,
            data: {},
            message: 'Room not found',
          });
      }

    } catch (e) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: e,
      });
    }
  });


  api.post('/:id/post',
    upload.single('coverImage')
    , async (req, res) => {
      try {
        const coverImage = req.file;
        console.log(coverImage);
        const room = await RoomModel.update({ _id: req.params.id }, {
          $push: {
            posts: {
              coverImage: coverImage,
              title: req.body.title,
              body: req.body.body,
              categories: req.body.categories
            },
          },
        });
        res.status(201)
          .json({
            isSuccess: true,
            data: room,
            message: 'Post Created!',
          });

      } catch (e) {
        res.status(500).json({
          isSuccess: false,
          message: 'Internal Server Error',
          data: e,
        });
      }

    });

  api.get('/:id/post-list', async (req, res) => {
    try {
      const room = await RoomModel.findOne({ _id: req.params.id });
      console.log(room.posts.filter(post => !post.deleted));
      res.status(200)
        .json({
          isSuccess: true,
          data: room.posts.filter(post => !post.deleted),
          message: 'Posts list',
        });

    } catch (e) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: e,
      });
    }

  });

  // api.get('/:componentId/list', async (req, res) => {
  //   try {
  //     const rooms = await RoomModel.find({ 'component._id': req.params.componentId });
  //     res.status(200)
  //       .json({
  //         isSuccess: true,
  //         data: rooms,
  //         message: 'All Rooms in the Components',
  //       });
  //   } catch (e) {
  //     res.status(500).json({
  //       isSuccess: false,
  //       message: 'Internal Server Error',
  //       data: e,
  //     });
  //   }
  // });

  api.get('/:id', async (req, res) => {
    try {
      const room = await RoomModel.findOne({ _id: req.params.id });
      res.status(201)
        .json({
          isSuccess: true,
          data: room,
          message: 'Room Details',
        });

    } catch (e) {
      res.status(500).json({
        isSuccess: false,
        message: 'Internal Server Error',
        data: e,
      });
    }

  });
  return api;
};
