const { Router } = require('express');

const { AuditLogModel } = require('../../models/auditLog');

module.exports = ({ db, config }) => {
  const api = Router();

  api.get('/', async (req, res) => {
    const variables = await Promise.all(['is_ambition', 'should_ambition', 'is_condition', 'should_condition'].map(async variable => {
      return {
        title: variable,
        likes: await AuditLogModel.count({ action: `like_variable_${variable}` }),
      };
    }));
    res.status(200)
      .json({
        isSuccess: true,
        data: variables,
        message: 'Variables'
      })
  });


  api.post('/:variable/like', async (req, res) => {
    try {
      await AuditLogModel.create({
        action: `like_variable_${req.params.variable}`,
        ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
      });
      const likes = await AuditLogModel.count({ action: `like_variable_${req.params.variable}` });
      res.status(200).json({
        isSuccess: true,
        data: likes,
        message: `Like count ${req.params.variable}`,
      });
    } catch (e) {
      res.status(500)
        .json({
          isSuccess: false,
          data: e,
          message: e.message,
        });
    }
  });

  api.get('/:variable/like', async (req, res) => {
    try {
      const likes = await AuditLogModel.count({ action: `like_variable_${req.params.variable}` });
      res.status(200).json({
        isSuccess: true,
        data: likes,
        message: `Like count ${req.params.variable}`,
      });
    } catch (e) {
      res.status(500)
        .json({
          isSuccess: false,
          data: e,
          message: e.message,
        });
    }
  });


  return api;
};
