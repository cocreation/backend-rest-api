const { Router } = require('express');
const { checkCategories, createCategoryA } = require('../../middlewares/CategoryA');
const { checkCategoryBList } = require('../../middlewares/CategoryB');
const { CategoryBModel } = require('../../models/categoryB');
const { CategoryAModel } = require('../../models/categoryA');
const { AuditLogModel } = require('../../models/auditLog');

module.exports = ({ db, config }) => {
  const api = Router();

  api.get('/', checkCategories, checkCategoryBList, (req, res) => {
    res.status(200)
      .json({
        isSuccess: true,
        data: {
          a: req.categoryAList,
          b: req.categoryBList,
        },
        message: 'List of Categories',
      });
  });

  api.get('/a-list', checkCategories, (req, res) => {
    res.status(200)
      .json({
        isSuccess: true,
        data: req.categoryAList,
        message: 'List of Category-A',
      });
  });

  api.get('/b-list', checkCategories, checkCategoryBList, (req, res) => {
    res.status(200)
      .json({
        isSuccess: true,
        data: req.categoryBList,
        message: 'List of Category-B',
      });
  });


  api.get('/a-list/:id/b-list', checkCategories, checkCategoryBList, async (req, res) => {
    try {
      const categoryA = await CategoryAModel.findById(req.params.id);
      if (categoryA) {
        console.log(categoryA);
        const categoriesForB = await CategoryBModel.find({ 'parentCategoryA._id': req.params.id });
        res.status(200)
          .json({
            isSuccess: true,
            data: categoriesForB,
            message: 'categories',
          });
      } else {
        res.status(404)
          .json({
            isSuccess: false,
            data: {},
            message: 'category not found',
          });
      }
    } catch (e) {
      res.status(500)
        .json({
          isSuccess: false,
          data: e,
          message: e.message,
        });
    }

  });

  api.post('/a/:id/like', async (req, res) => {
    try {
      const categoryA = await CategoryAModel.findById(req.params.id);
      if (categoryA) {
        categoryA.likes += 1;
        await categoryA.save();
        await CategoryBModel.updateMany({ 'parentCategoryA._id': req.params.id }, {
          parentCategoryA: categoryA,
        });
        await AuditLogModel.create({
          action: 'like_category_a',
          ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
          object: categoryA._id,
        });
        res.status(200)
          .json({
            isSuccess: true,
            data: categoryA,
            message: 'liked!',
          });
      } else {
        res.status(404)
          .json({
            isSuccess: false,
            data: {},
            message: 'category not found',
          });
      }
    } catch (e) {
      res.status(500)
        .json({
          isSuccess: false,
          data: e,
          message: e.message,
        });
    }
  });

  api.post('/b/:id/like', async (req, res) => {
    try {
      const categoryB = await CategoryBModel.findById(req.params.id);
      if (categoryB) {
        categoryB.likes += 1;
        await categoryB.save();
        await AuditLogModel.create({
          action: 'like_category_b',
          ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
          object: categoryB._id,
        });
        res.status(200)
          .json({
            isSuccess: true,
            data: categoryB,
            message: 'liked!',
          });
      } else {
        res.status(404)
          .json({
            isSuccess: false,
            data: {},
            message: 'category not found',
          });
      }
    } catch (e) {
      res.status(500)
        .json({
          isSuccess: false,
          data: e,
          message: e.message,
        });
    }
  });

  return api;
};
