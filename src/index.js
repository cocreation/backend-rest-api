const express = require('express');
const BodyParser = require('body-parser');
const passport = require('passport');
const config = require('./config');
//const { User } = require('./models/user');
const VersionOneRouter = require('./routes/v1');
const app = express();

app.use(BodyParser.json({ limit: '50mb' }));
app.use(BodyParser.urlencoded({ extended: true, limit: '50mb' }));

//passport.use(User.createStrategy());
//passport.serializeUser(User.serializeUser());
//passport.deserializeUser(User.deserializeUser());

app.use('/static', express.static('./src/public'));

app.use('/v1', VersionOneRouter);

app.listen(config.applicationPort, () => {
  console.log(`Listening on ${config.applicationPort}...`);
});
